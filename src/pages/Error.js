import { Link } from "react-router-dom";


export default function Error() {

	return (
		<>
		<section className = 'section'>
			<h2>Zuitt Booking</h2>
			<p>Page Not Found!</p>
			<Link to = '/'>Back To Homepage</Link>
		</section>
		</>
	);
};