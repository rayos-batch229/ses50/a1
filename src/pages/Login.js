import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Login() {

// Allow us to cosnume the User Context object and its properties
  const {user, setUser} = useContext(UserContext);

  
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');

  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password1);


  useEffect(() => {

      if((email !== '' && password1 !== '')){
        setIsActive(true);
      } else {
        setIsActive(false);
      }
  })

  function authentication(e) {
      e.preventDefault();

      // Process a fetch request to the corresponding backend API
      /*
          Syntax:
              fetch('url', {options})
              .then(res => res.json())
              .then(data => {})
      */
      fetch('http://localhost:4000/users/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              email: email,
              password: password1
          })
      })
      .then(res => res.json())
      .then(data => {

            console.log(data)

            // If no user information is found, the "access" property will not be available and will return undefined

            // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type

            if(typeof data.access !== "undefined") {
              localStorage.setItem('token', data.access);
              retrieveUserDetails(data.access);

              Swal.fire({
                  title: "Login Successfull",
                  icon: "success",
                  text: "Welcome to Zuitt"
              })
            } else {
              Swal.fire({
                  title: "Authentication Failed",
                  icon: "error",
                  text: "Check Your Login Details"
              })
            }
      })

      localStorage.setItem('email', email)

  setUser({
        email: localStorage.getItem('email')
      })

 // alert('Success!')

      setEmail('');
      setPassword1('');    
  }

  const retrieveUserDetails = (token) => {
    // The token will be sent as part of the request's header information
    // We put "Bearer" in front of the token to follow implementation standards for JWTs

    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
          console.log(data)

          // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
    })
  }

   return (

    (user.id !== null) ? 
    <Navigate to='/courses'/> 
    :
    <>
      <h1> Login </h1>
      <Form onSubmit={(e) => authentication(e)}>

        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password"  value={password1} onChange={e => setPassword1(e.target.value)} required/>
        </Form.Group>

        {
        (isActive) ?
        <Button variant="primary" type="submit" controlId="submitBtn">
          Login
        </Button>
        :
        <Button variant="primary" type="submit" controlId="submitBtn" disabled>
          Login
        </Button>
        }

      </Form>
    </>
      );
}